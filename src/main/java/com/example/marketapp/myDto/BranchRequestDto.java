package com.example.marketapp.myDto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchRequestDto {
     Long id;
     String branchName;
     Long marketId;
     Long addressId;





}
