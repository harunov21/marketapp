package com.example.marketapp.myDto;

import com.example.marketapp.Model.Address;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PersonRequestDto {
    Long id;
    String personName;
    Long addressId;
}
