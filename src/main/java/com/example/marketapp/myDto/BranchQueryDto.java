package com.example.marketapp.myDto;

import lombok.*;




public class BranchQueryDto {
    private Long branchId;
    private String branchName;
    private String addressName;
    private String marketName;
    private String addressCity;
    private String addressStreet;

    public BranchQueryDto() {
    }

    public BranchQueryDto(Long branchId, String branchName, String addressName, String marketName, String addressCity, String addressStreet) {
        this.branchId = branchId;
        this.branchName = branchName;
        this.addressName = addressName;
        this.marketName = marketName;
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
    }

    public Long getBranchId() {
        return branchId;
    }

    public void setBranchId(Long branchId) {
        this.branchId = branchId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getAddressName() {
        return addressName;
    }

    public void setAddressName(String addressName) {
        this.addressName = addressName;
    }

    public String getMarketName() {
        return marketName;
    }

    public void setMarketName(String marketName) {
        this.marketName = marketName;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }
}

