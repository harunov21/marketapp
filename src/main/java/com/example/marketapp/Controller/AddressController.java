package com.example.marketapp.Controller;

import com.example.marketapp.myDto.AddressRequestDto;
import com.example.marketapp.Model.Address;
import com.example.marketapp.Service.AddressService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/adress")
@RestController
@RequiredArgsConstructor
public class AddressController {
    private final AddressService addressService;


    @GetMapping("/all")
    public List<Address> getAllAdresses(){
        return addressService.findAll();
    }

    @PostMapping("/create")
    public Long createAddress(@RequestBody AddressRequestDto addressRequestDto) {
       return addressService.createAddress(addressRequestDto);

}
    @PutMapping("/{id}")
    public Long updateAdress(@Valid @RequestBody AddressRequestDto addressRequestDto,
                             @Valid @PathVariable Long id) {
        return addressService.updateAdress(addressRequestDto, id);

}
    @DeleteMapping("/{id}")
    public void deleteAdress(@Valid @PathVariable Long id) {
        addressService.deleteAdress(id);
    }





}