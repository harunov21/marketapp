package com.example.marketapp.Controller;

import com.example.marketapp.Service.PersonService;
import com.example.marketapp.myDto.PersonRequestDto;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/person")
@RequiredArgsConstructor
public class PersonController {
    private final PersonService personService;

    @GetMapping("/all")
    public List<PersonRequestDto> getAllPerson (){
        return personService.getAll();
    }

    @PostMapping("/create")
    public Long savePerson (@Valid @RequestBody PersonRequestDto personRequestDto){
        return personService.save(personRequestDto);
    }

}
