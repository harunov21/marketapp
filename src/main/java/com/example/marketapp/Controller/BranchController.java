package com.example.marketapp.Controller;

import com.example.marketapp.myDto.BranchQueryDto;
import com.example.marketapp.myDto.BranchRequestDto;
import com.example.marketapp.Model.Branch;
import com.example.marketapp.Service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/branches")
public class BranchController {

    private final BranchService branchService;

    @GetMapping("/branch")
    public BranchQueryDto getBranch (){
        return branchService.getBranch();
    }

    @GetMapping("/details")
    public List<BranchQueryDto> getAllBranches(){
        return branchService.getAllBranches();
    }

    @PostMapping("/create")
    public Long createBranch(@RequestBody BranchRequestDto branchRequestDto) {
        return branchService.createBranch(branchRequestDto);
    }

    @PutMapping("/{id}")
    public Long updateBranch(@Valid @RequestBody BranchRequestDto branchRequestDto,
                                         @Valid @PathVariable Long id) {
        return branchService.updateAdress(branchRequestDto, id);
    }

    @DeleteMapping("/{id}")
    public void deleteBranch(@Valid @PathVariable Long id) {
        branchService.deleteBranch(id);
    }
}


