package com.example.marketapp.Controller;

import com.example.marketapp.myDto.MarketRequestDto;
import com.example.marketapp.Service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/markets")
public class MarketController {
    private final MarketService marketService;

    @GetMapping()
    public List<MarketRequestDto> getMarket() {
        return marketService.getMarket();
    }
    @PostMapping("/create")
    public Long createMarket (@Valid @RequestBody MarketRequestDto marketRequestDto){
       return marketService.saveMarket(marketRequestDto);
    }
    @PutMapping("/{id}")
    public Long updateMarket (@Valid @RequestBody MarketRequestDto marketRequestDto,
                                         @Valid @PathVariable Long id){
       return marketService.updatemarket(marketRequestDto,id);
    }

    @DeleteMapping("/{id}")
    public void deleteMarket(@Valid @PathVariable Long id){
        marketService.deleteMarket(id);

    }

}
