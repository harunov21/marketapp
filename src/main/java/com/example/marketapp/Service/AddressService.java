package com.example.marketapp.Service;

import com.example.marketapp.myDto.AddressRequestDto;
import com.example.marketapp.Model.Address;

import java.util.List;

public interface AddressService {

    Long createAddress(AddressRequestDto addressRequestDto);


    List<Address> findAll();

    Long updateAdress(AddressRequestDto addressRequestDto, Long id);

    void deleteAdress(Long id);

    Address finById(Long id);
}
