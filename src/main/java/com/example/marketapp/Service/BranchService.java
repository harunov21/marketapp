package com.example.marketapp.Service;

import com.example.marketapp.myDto.BranchQueryDto;
import com.example.marketapp.myDto.BranchRequestDto;
import com.example.marketapp.Model.Branch;

import java.util.List;

public interface BranchService {
    
    List<BranchQueryDto> getAllBranches();

    Long createBranch(BranchRequestDto branchRequestDto);


    void deleteBranch(Long id);

    Long updateAdress(BranchRequestDto branchRequestDto, Long id);

    BranchQueryDto getBranch();
}
