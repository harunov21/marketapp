package com.example.marketapp.Service;

import com.example.marketapp.myDto.MarketRequestDto;
import com.example.marketapp.Model.Market;

import java.util.List;

public interface MarketService {
    

    Long updatemarket(MarketRequestDto marketRequestDto, Long id);

    void deleteMarket(Long id);

    Long saveMarket(MarketRequestDto marketRequestDto);

    Market findBy (Long id);

    List<MarketRequestDto> getMarket();
}
