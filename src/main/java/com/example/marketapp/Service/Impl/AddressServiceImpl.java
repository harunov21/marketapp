package com.example.marketapp.Service.Impl;

import com.example.marketapp.Configuration.Config;
import com.example.marketapp.myDto.AddressRequestDto;
import com.example.marketapp.Exception.AddressNotFoundException;
import com.example.marketapp.Model.Address;
import com.example.marketapp.Repository.AddressRepository;
import com.example.marketapp.Service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {
    private final AddressRepository addressRepository;
    private final Config config;


    @Override
    public Long createAddress(AddressRequestDto addressRequestDto) {
        Address adress = config.getMapper().map(addressRequestDto, Address.class);
        return addressRepository.save(adress).getId();
    }

    @Override
    public List<Address> findAll() {
       return addressRepository.findAll();
    }

    @Override
    public Long updateAdress(AddressRequestDto addressRequestDto, Long id) {
        Address adress = addressRepository.findById(id)
                .orElseThrow(() -> new AddressNotFoundException(id));
        adress.setAddressName(addressRequestDto.getAddressName());
        addressRepository.save(adress);

        return id;
    }

    @Override
    public void deleteAdress(Long id) {
        addressRepository.deleteById(id);
    }

    @Override
    public Address finById(Long id) {
        Optional<Address> byId = addressRepository.findById(id);
        byId.orElseThrow(() -> new RuntimeException());
        return byId.get();


    }


}
