package com.example.marketapp.Service.Impl;

import com.example.marketapp.Configuration.Config;
import com.example.marketapp.myDto.MarketRequestDto;
import com.example.marketapp.Exception.MarketNotFoundExpception;
import com.example.marketapp.Model.Market;
import com.example.marketapp.Repository.MarketRepository;
import com.example.marketapp.Service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {
    private final MarketRepository marketRepository;
    private final Config config;

    @Override
    public Long saveMarket(MarketRequestDto marketRequestDto) {
        Market market = config.getMapper().map(marketRequestDto, Market.class);
        return marketRepository.save(market).getId();

    }

    @Override
    public Market findBy(Long id) {
        Optional<Market> byId = marketRepository.findById(id);
        byId.orElseThrow(()-> new RuntimeException());
        return byId.get();
    }

    @Override
    public List<MarketRequestDto> getMarket() {
        List<Market> marketList = marketRepository.findAll();
        List<MarketRequestDto> marketResponseDtoList = new ArrayList<>();
        marketList.forEach(entity -> {
            MarketRequestDto marketResponseDto =
                    config.getMapper().map(entity, MarketRequestDto.class);
            marketResponseDtoList.add(marketResponseDto);
        });
        return marketResponseDtoList;
    }


    @Override
    public Long updatemarket(MarketRequestDto marketRequestDto, Long id) {
        Market market = marketRepository.findById(id)
                .orElseThrow(() -> new MarketNotFoundExpception(id));

        market.setMarketName(marketRequestDto.getName());
        marketRepository.save(market);

        return id;
    }
    @Override
    public void deleteMarket(Long id) {
        marketRepository.deleteById(id);
    }


}
