package com.example.marketapp.Service.Impl;

import com.example.marketapp.Configuration.Config;
import com.example.marketapp.myDto.BranchQueryDto;
import com.example.marketapp.myDto.BranchRequestDto;
import com.example.marketapp.Exception.BranchNotFoundException;
import com.example.marketapp.Model.Address;
import com.example.marketapp.Model.Branch;
import com.example.marketapp.Model.Market;
import com.example.marketapp.Repository.BranchRepository;
import com.example.marketapp.Service.AddressService;
import com.example.marketapp.Service.BranchService;
import com.example.marketapp.Service.MarketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;
    private final Config config;
    private final AddressService addressService;
    private final MarketService marketService;


    public List<BranchQueryDto> getAllBranches() {
        return branchRepository.getAllBranches();
    }

    @Override
    public Long createBranch(BranchRequestDto branchRequestDto) {
        Address address = addressService.finById(branchRequestDto.getAddressId());
        Market market = marketService.findBy(branchRequestDto.getMarketId());
        Branch branch = Branch.builder()
                .branchName(branchRequestDto.getBranchName())
                .address(address)
                .market(market)
                .build();
        return branchRepository.save(branch).getId();
    }

    @Override
    public Long updateAdress(BranchRequestDto branchRequestDto, Long id) {
        Branch branch = branchRepository.findById(id)
                .orElseThrow(() -> new BranchNotFoundException(id));

        branch.setBranchName(branchRequestDto.getBranchName());
        branchRepository.save(branch);

        return id;
    }

    @Override
    public BranchQueryDto getBranch() {
//        Branch branch = branchRepository.getBranchNativeQuery();
        BranchQueryDto branchQueryDto = branchRepository.getBranchNativeQuery();
        log.info("Getting branch from db : {}" , branchQueryDto);
        return branchQueryDto;
    }

    @Override
    public void deleteBranch(Long id) {
        branchRepository.deleteById(id);

    }


}






