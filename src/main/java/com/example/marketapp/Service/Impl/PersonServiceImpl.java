package com.example.marketapp.Service.Impl;

import com.example.marketapp.Model.Address;
import com.example.marketapp.Model.Market;
import com.example.marketapp.Model.Person;
import com.example.marketapp.Repository.AddressRepository;
import com.example.marketapp.Repository.PersonRepository;
import com.example.marketapp.Service.AddressService;
import com.example.marketapp.Service.PersonService;
import com.example.marketapp.myDto.PersonRequestDto;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {
    private final PersonRepository personRepository;
    private final ModelMapper modelMapper;
    private final AddressService addressService;
    @Override
    public List<PersonRequestDto> getAll() {
        List<Person> people = personRepository.findAll();
        List<PersonRequestDto> requestDto = new ArrayList<>();
        people.forEach(person -> {
            PersonRequestDto personRequestDto = modelMapper.map(person,PersonRequestDto.class);
            requestDto.add(personRequestDto);
        });
        return requestDto;
    }

    @Override
    public Long save(PersonRequestDto personRequestDto) {
        Address address = addressService.finById(personRequestDto.getId());
        Person person = Person.builder()
                .name(personRequestDto.getName())
                .address(address)
                .build();
        return personRepository.save(person).getId();
    }


    }



