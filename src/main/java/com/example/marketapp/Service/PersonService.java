package com.example.marketapp.Service;

import com.example.marketapp.Model.Person;
import com.example.marketapp.myDto.PersonRequestDto;

import java.util.List;

public interface PersonService {
    List<PersonRequestDto> getAll();

    Long save(PersonRequestDto personRequestDto);


}
