package com.example.marketapp.Repository;

import com.example.marketapp.Model.Market;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MarketRepository extends JpaRepository<Market,Long> {

}
