package com.example.marketapp.Repository;

import com.example.marketapp.Model.Branch;
import com.example.marketapp.myDto.BranchQueryDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BranchRepository extends JpaRepository<Branch,Long> {


    @Query(value = "SELECT new com.example.marketapp.myDto.BranchQueryDto( b.id, b.branchName, a.addressName, m.marketName,a.city,a.street )" +
            " from Branch b JOIN b.market m JOIN b.address a")
    BranchQueryDto getBranchNativeQuery();

    @Query(value = "select new com.example.marketapp.myDto.BranchQueryDto(b.id, b.branchName, a.addressName, m.marketName,a.city,a.street) FROM Branch b JOIN b.market m JOIN b.address a")
    List<BranchQueryDto> getAllBranches();


//    @Query(value = "select b from Branch b join fetch b.market m join fetch b.address a ")
//    Branch getBranchJPql();
}


