package com.example.marketapp;

import com.example.marketapp.Repository.BranchRepository;
import com.example.marketapp.myDto.BranchQueryDto;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@RequiredArgsConstructor
@SpringBootApplication
public class MarketappApplication implements CommandLineRunner {
    private final BranchRepository branchRepository;

    public static void main(String[] args) {
        SpringApplication.run(MarketappApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<BranchQueryDto> getbranches = branchRepository.getAllBranches();
        for (BranchQueryDto branch : getbranches){
            System.out.println("Branch ID: " + branch.getBranchId());
            System.out.println("Branch Name: " + branch.getBranchName());
            System.out.println("Address Name: " + branch.getAddressName());
            System.out.println("Market Name: " + branch.getMarketName());        }
    }
}
