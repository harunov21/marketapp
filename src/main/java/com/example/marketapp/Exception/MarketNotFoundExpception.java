package com.example.marketapp.Exception;

public class MarketNotFoundExpception extends RuntimeException{
    public MarketNotFoundExpception(Long id) {
        super("Market not found with id: " + id);
    }
}

