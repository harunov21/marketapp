package com.example.marketapp.Exception;

public class BranchNotFoundException extends RuntimeException{
    public BranchNotFoundException(Long id) {
        super("Branch not found with id: " + id);
    }
}

