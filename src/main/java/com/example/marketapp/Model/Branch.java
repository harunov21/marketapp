package com.example.marketapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    String branchName;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "a_id", referencedColumnName = "id")
    @JsonIgnore
    Address address;

    @ManyToOne
    @JoinColumn(name = "m_id", referencedColumnName = "id")
    @JsonIgnore
    Market market;
}
