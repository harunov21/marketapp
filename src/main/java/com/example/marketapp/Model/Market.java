package com.example.marketapp.Model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Market {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     Long id;
    String marketName;

    @OneToMany(mappedBy = "market", cascade = CascadeType.ALL)
    @JsonIgnore
    @ToString.Exclude
    List<Branch> branch;

   }
