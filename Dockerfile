FROM openjdk:17
COPY build/libs/Marketapp-0.0.1-SNAPSHOT.jar /market-app/
CMD ["java", "-jar", "/market-app/Marketapp-0.0.1-SNAPSHOT.jar"]